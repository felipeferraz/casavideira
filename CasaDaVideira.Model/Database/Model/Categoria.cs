﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace CasaDaVideira.Model.Database.Model
{
    public class Categoria
    {
        public virtual int IdCategoria { get; set; }
        public virtual string Nome { get; set; }
        public virtual Produto Produto { get; set; }
    }

    public class CategoriaMap : ClassMapping<Categoria>
    {
        public CategoriaMap()
        {
            //esta mapeando uma primarykey
            Id(x => x.IdCategoria, m => m.Generator(Generators.Identity));

            Property(x => x.Nome);

            //OneToMany(x => x.Produto, m =>
            //{
            //    m.Column("idProduto");
            //});

        }
    }
}